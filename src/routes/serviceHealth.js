import {Router} from 'express';
import {testEndpoint} from '../controllers/serviceHealth';
import passport from '../middlewares/passport';

const usersRouter = new Router();

usersRouter.get('/test', passport.authorize('jwt', {session: false}), testEndpoint);

export default usersRouter;
