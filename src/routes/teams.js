import {Router} from 'express';
import multer from '../middlewares/multer';
import {
  addTeam,
  inviteTeamMembers,
  updateTeamInvitation,
  refuseTeamInvitation,
  acceptTeamInvitation,
  pendTeamInvitation,
  removeTeamMembers
} from '../controllers/teams';

const teamRouter = new Router();

teamRouter.post('/add-team', multer.single('logo'), addTeam);
teamRouter.post('/invite-team-member', inviteTeamMembers);
teamRouter.post('/update-team-invitation', updateTeamInvitation);
teamRouter.post('/remove-team-member', removeTeamMembers);
teamRouter.post('/refuse-team-invitation/:invitationId', refuseTeamInvitation);
teamRouter.post('/accept-team-invitation/:invitationId', acceptTeamInvitation);
teamRouter.post('/pend-team-invitation/:invitationId', pendTeamInvitation);


export default teamRouter;
