import {Router} from 'express';
import {
  addGoalToMatch,
  addMatch,
  allMatches,
  getMatch,
  paginateMatches,
  updateMatchScore
} from '../controllers/matches';

const matchRouter = new Router();

matchRouter.get('/', allMatches);
matchRouter.get('/paginate', paginateMatches);
matchRouter.get('/:matchId', getMatch);
matchRouter.post('/add-match', addMatch);
matchRouter.post('/add-goal-to-match/:matchId', addGoalToMatch);
matchRouter.post('/update-match-score/:matchId', updateMatchScore);

export default matchRouter;
