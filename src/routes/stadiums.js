import {Router} from 'express';
import multer from '../middlewares/multer';
import {addStadium, allStadiums, getStadium} from '../controllers/stadiums';

const stadiumRouter = new Router();

stadiumRouter.get('/', allStadiums);
stadiumRouter.get('/:stadiumId', getStadium);
stadiumRouter.post('/add-stadium', multer.any(), addStadium);

export default stadiumRouter;
