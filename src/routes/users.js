import {Router} from 'express';
import {register, logIn} from '../controllers/users';
import {allPlayers, getPlayer} from '../controllers/players';

const usersRouter = new Router();

usersRouter.post('/register', register);
usersRouter.post('/login', logIn);
usersRouter.get('/players', allPlayers);
usersRouter.get('/players/:playerId', getPlayer);

export default usersRouter;
