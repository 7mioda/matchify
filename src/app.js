import Express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import healthRouter from './routes/serviceHealth';
import usersRouter from './routes/users';
import stadiumRouter from './routes/stadiums';
import matchRouter from './routes/matches';
import teamRouter from './routes/teams';

const app = new Express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('combined'));

app.use('/api/v1/health', healthRouter);
app.use('/api/v1/users', usersRouter);
app.use('/api/v1/stadiums', stadiumRouter);
app.use('/api/v1/matches', matchRouter);
app.use('/api/v1/teams', teamRouter);

app.use(cors());


export default app;
