import chalk from 'chalk';
import app from './app';

import 'dotenv/config';
import serverResolver from './serverResolver';
import database from './models/database';


const port = process.env.PORT || 7000;

database().then(() => {

    const server = serverResolver(app);

    server.listen({port}, (error) => {

        if (error) {

            console.log(chalk.white.bgRedBright(`Server Error: ${error}`));

        } else {

            console.log(chalk.white.bgGreen(`Go Go server on ${port}`));

        }

    });

});


