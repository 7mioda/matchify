import nodemailer from 'nodemailer';
import Twig from 'twig';


const mailerRender = (path, options) => new Promise((resolve, reject) => {

        Twig.renderFile(path, options, (err, html) => {

            if (err) {

                reject(err);

            }
            resolve(html);

        });

    }),

    {MAILER_HOST: mailerHost,
  MAILER_USER: user,
  MAILER_PASS: pass} = process.env,

    transporter = nodemailer.createTransport({
        service: mailerHost,
        auth: {
            user,
            pass
        }
    });

/**
 *
 * @param to {string | Array } list of receivers
 * @param subject {string} Subject line
 * @param template {string} path for email twig template
 * @param data {object} data for twig template
 * @returns {Promise<void>}
 */
export async function sendMail (to, subject, template, data) {

    try {

        const html = await mailerRender(template, data);

        await transporter.sendMail({
            from: user,
            to, // List of receivers
            subject, // Subject line
            html // Html body
        });

    } catch (error) {

        console.error(error);

    }

}

/**
 *
 * @param to {string | Array } list of receivers
 * @param data {object} for twig template
 * @returns {Promise<void>}
 */
export async function sendRegistrationMail (to, data) {

    await sendMail(to, 'Registration mail', './src/services/registration.twig', data);

}
