import {Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt';
import passport from 'passport';
import User from '../models/user';

const middleWare = (pass) => {

    const opts = {};

    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
    opts.secretOrKey = 'app-super-secret';
    pass.use(
    new JwtStrategy(opts, async ({user}, done) => {

        try {

            const user = await User.findById(user);


            return user ? done(null, user) : done(null, false);

        } catch (error) {

            return done(error, false);

        }

    })
  );

};

middleWare(passport);

export default passport;
