import cloudinary from 'cloudinary';
import cloudinaryStorage from 'multer-storage-cloudinary';
import multer from 'multer';
import shortid from 'shortid';

cloudinary.config({
    cloud_name: 'oyez',
    api_key: '684362786892363',
    api_secret: 'uNzObx8HRTxeDvIHJVo72s1J2kU'
});

const storage = cloudinaryStorage({
    cloudinary,
    folder: 'samples',
    filename: (request, file, callback) => {

        const {originalname} = file,
            cloudName = `${originalname}-${shortid.generate()}`;

        callback(request, cloudName);

    }
});

export default multer({storage});
