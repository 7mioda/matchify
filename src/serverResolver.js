import https from 'https';
import fs from 'fs';
import http from 'http';
import chalk from 'chalk';
import configuration from '../config';


const environment = process.env.NODE_ENV || 'development',
    config = configuration[environment];

/**
 *
 * @param  {Express} app express App
 * @returns {null|Server | Http2Server} http|https express server
 */
export default function serverResolver (app) {

    const {ssl} = config;

    if (ssl) {

        try {

            return https.createServer(
                {
                    cert: fs.readFileSync(`./ssl/${environment}/server.crt`),
                    key: fs.readFileSync(`./ssl/${environment}/server.key`)
                },
                app
            );

        } catch (error) {

            console.log(
              chalk.greenBright.bgRed('something went wrong with ssl cert')
            );

            return null;

        }

    } else {

        return http.createServer(app);

    }

}
