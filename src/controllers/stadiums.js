import {pipeline as callbackPipeline} from 'stream';
import {promisify} from 'util';
import { map } from 'lodash';
import * as JSONStream from 'JSONStream';
import Stadium from '../models/stadium';

const pipeline = promisify(callbackPipeline);

/**
 *
 * @param request
 * @param response
 * @returns {Promise<void>}
 */
export const addStadium = async (request, response) => {

    try {

        const {body, files} = request;
        const {photos: bodyPhotos, facilities} = body;
        const photos = files
      ? files.filter(({fieldname}) => fieldname === 'photos').map(({url}) => url)
      : bodyPhotos;
        const withPhotosFacilities = map(facilities, (facility, index) => ({
            ...facility,
            photos: map(files.filter(({fieldname}) => fieldname === `facilities[${index}][photos]`),({url}) => url) || facility.photos
        }));
        const stadium = Stadium({...body,
            photos,
            facilities: withPhotosFacilities});

        await stadium.save();
        response.
      status(201).
      json({
          stadium
      });

    } catch (error) {

        console.error(`Failed to insert stadium: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 *
 * @param request
 * @param response
 */
export const allStadiums = async (request, response) => {

    try {

        await pipeline(
      Stadium.find({}).stream(),
      JSONStream.stringify(),
      response.type('json')
    );

    } catch (error) {

        console.error(`Failed to fetching stadiums: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 *
 * @param request
 * @param response
 * @returns {Promise<void>}
 */
export const getStadium = async (request, response) => {

    try {

        const {params: {stadiumId}} = request;
        const stadium = await Stadium.findOne({_id: stadiumId});

        response.
      status(200).
      json({
          stadium
      });

    } catch (error) {

        console.log(`Failed to fetch stadium: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};
