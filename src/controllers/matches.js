import {isEqual} from 'lodash';
import Match from '../models/match';
import Goal from '../models/goal';
import * as JSONStream from 'JSONStream';

/**
 * Add new Match
 *
 * @param request http request for adding new match
 * @param response http response
 * @returns {Promise<void>}
 *  201 { match } as Data if match was added successfully
 *  500 { message }
 */
export const addMatch = async (request, response) => {

    try {

        const {body} = request;
        const match = Match(body);

        await match.save();
        response.
      status(201).
      json({
          match
      });

    } catch (error) {

        console.log(`Failed to insert match: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Fetch all Matches
 *
 * @param request http request to fetch all matches
 * @param response http response
 * @returns {Promise<void>}
 *  200 { matches } as Data
 *  500 { message }
 */
export const allMatches = (request, response) => {

    try {

        response.status(200);
        Match.find().
      stream().
      pipe(JSONStream.stringify()).
      pipe(response.type('json'));

    } catch (error) {

        console.log(`Failed to fetch matches: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Paginate through all Matches
 *
 * @param request http request to paginate through matches
 *  offset & limit as queryParams to specify which data block to be fetched
 * @param response http response
 * @returns {Promise<void>}
 *  200 { matches } as Data
 *  500 { message }
 */
export const paginateMatches = async (request, response) => {

    try {

        const {query: {offset, limit}} = request;
        const matches = await Match.find({}).
      skip(parseFloat(offset)).
      limit(parseFloat(limit));

        response.
        status(200).
        json({
            matches
        });

    } catch (error) {

        console.log(`Failed to paginate matches: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Add a goal to Match
 *
 * @param request http request to add new goal to match
 * @param response http response
 * @returns {Promise<void>}
 *  200 { match } as Data
 *  422 if match is not found
 *  500 { message }
 */
export const addGoalToMatch = async (request, response) => {

    try {

        const {params: {matchId}, body: {goal: goalData}} = request;
        const match = await Match.findById(matchId).populate('teams');

        if (!match) {

            return response.sendStatus(422);

        }
        const {player} = goalData;
        const {teams, score} = match;
        const scoredTeam = teams.find(({members}) => members.includes(player));
        const newScore = score.length > 0
      ? score.map((single) => {

          if (isEqual(single.team, scoredTeam._id)) {

              return {team: single.team,
                  score: parseFloat(single.score) + 1};

          }

          return single;

      })
      : [
          {team: scoredTeam._id,
              score: 1}
      ];
        const goal = new Goal({match: matchId,
            ...goalData});

        await goal.save();
        const {_id: goalId} = goal;
        const updatedMatch = await Match.findOneAndUpdate({_id: match._id}, {$push: {goals: goalId},
            score: [...newScore],
            updatedAt: new Date()}, {new: true});

        return response.
      status(200).
      json({
          match: updatedMatch
      });

    } catch (error) {

        console.error(`Failed to update score(add goal): ${error}`);
        const {message} = error;

        return response.
      status(500).
      json({
          message
      });

    }

};


/**
 * Update the score for specific match
 *
 * @param request http request to update the score
 * @param response http response
 * @returns {Promise<void>}
 *  200 { match } as Data
 *  500 { message }
 */
export const updateMatchScore = async (request, response) => {

    try {

        const {params: {matchId}, body: {score}} = request;

        await Match.findByIdAndUpdate(matchId, {score,
            updatedAt: new Date()});
        response.sendStatus(200);

    } catch (error) {

        console.log(`Failed to update match score: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Fetch a specific match
 *
 * @param request http request to fetch a specific match
 * @param response http response
 * @returns {Promise<void>}
 *  200 { match } as Data
 *  500 { message }
 */
export const getMatch = async (request, response) => {

    try {

        const {params: {matchId}} = request;
        const match = await Match.findOne({_id: matchId});

        response.
      status(200).
      json({
          match
      });

    } catch (error) {

        console.log(`Failed to fetch match: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};
