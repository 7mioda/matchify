/**
 * End-point to test the service
 *
 * @param {Request} request simple http request for test
 * @param {Response} response http response
 * @returns {Promise<Response>} returns ok as Data
 * and 200 as status || 500 if there's an error
 */
export const testEndpoint = (request, response) => {

    try {

        response.status(200).json({
            data: 'OK'
        });

    } catch ({message}) {

        response.status(500).json({
            error: message
        });

    }

};
