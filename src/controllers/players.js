import * as JSONStream from 'JSONStream';
import User from '../models/user';


export const allPlayers = (request, response) => {

    try {

        User.find({}, {password: 0}).
      stream().
      pipe(JSONStream.stringify()).
      pipe(response.type('json'));

    } catch (error) {

        console.error(`Failed to fetching players: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

export const getPlayer = async (request, response) => {

    try {

        const {params: {playerId}} = request,
            player = await User.findOne({_id: playerId}, {password: 0,
                firstConnection: 0});

        response.
      status(200).
      json({
          player
      });

    } catch (error) {

        console.error(`Failed to fetching player: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};
