import Team from '../models/team';
import Invitation from '../models/invitation';

/**
 * Add a team
 *
 * @param {Request} request http request for adding new team
 * @param {Response} response http response
 * @returns {Promise<Response>} returns
 *  201 { team } as Data if team was added successfully
 *  500 { message }
 *
 */
export const addTeam = async (request, response) => {

    try {

        const {body, file} = request;

        if (file) {

            body.logo = file.url;

        }
        const team = Team({...body,
            members: []});

        await team.save();
        const {members, captain, _id} = team;
        const invitationsPromise = members.map((member) => new Invitation({captain,
            team: _id,
            member}).save());

        await Promise.all(invitationsPromise);
        response.
      status(201).
      json({
          team
      });

    } catch (error) {

        console.error(`Failed to adding team: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Invite members to join a team
 *
 * @param {Request} request http request for adding new team members invitations
 * @param {Response} response http response
 * @returns {Promise<Response>} returns
 *  200 if all invitation has been added successfully
 *  500 { message }
 *
 */
export const inviteTeamMembers = async (request, response) => {

    try {

        const {body: {members, captain, team}} = request;
        const invitationsPromise = members.map((member) => new Invitation({captain,
            team,
            member}).save());

        await Promise.all(invitationsPromise);
        response.sendStatus(200);

    } catch (error) {

        console.error(`failed to send invitations: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Remove members from a team
 *
 * @param {Request} request http request for removing members from a team
 * @param {Response} response http response
 * @returns {Promise<Response>} returns
 *  200  if members was removed successfully
 *  500 { message }
 *
 */
export const removeTeamMembers = async (request, response) => {

    try {

        const {body: {team, members}} = request;

        await Team.findByIdAndUpdate({_id: team}, {$pullAll: {members},
            updatedAt: Date.now()});
        response.sendStatus(200);

    } catch (error) {

        console.error(`failed to remove team members: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Update invitation status
 *
 * @param {Request} request http request for updating invitation status
 * @param {Response} response http response
 * @returns {Promise<Response>} returns
 *  200  if invitation's status was updated successfully
 *  500 { message }
 *
 */
export const updateTeamInvitation = async (request, response) => {

    try {

        const {body: {invitation, status}} = request;

        await Invitation.findByIdAndUpdate({_id: invitation}, {status,
            updatedAt: Date.now()});
        response.sendStatus(200);

    } catch (error) {

        console.error(`failed to update invitation status: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Refuse team invitation
 *
 * @param {Request} request http request to refuse team invitation
 * @param {Response} response http response
 * @returns {Promise<Response>} returns
 *  200  if invitation's status was updated to refused successfully
 *  500 { message }
 *
 */
export const refuseTeamInvitation = async (request, response) => {

    try {

        const {params: {invitationId}} = request;

        await Invitation.findByIdAndUpdate({_id: invitationId}, {status: 'refused',
            updatedAt: Date.now()});
        response.sendStatus(200);

    } catch (error) {

        console.error(`failed to refuse invitations: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Accept team invitation
 *
 * @param {Request} request http request to accept team invitation
 * @param {Response} response http response
 * @returns {Promise<Response>} returns
 *  200  if invitation's status was updated to accepted successfully
 *  422 if invitation was already accepted or refused
 *  500 { message }
 *
 */
export const acceptTeamInvitation = async (request, response) => {

    try {

        const {params: {invitationId}} = request;
        const invitation = await Invitation.findById(invitationId);
        const {status} = invitation;

        if (status === 'pending') {

            await invitation.updateOne({status: 'accepted',
                updatedAt: Date.now()});
            const {team, member} = invitation;

            await Team.findByIdAndUpdate({_id: team}, {$push: {members: member},
                updatedAt: Date.now()});
            response.sendStatus(200);

        } else {

            const message = status === 'accepted'
        ? 'invitation already accepted'
        : `cannot change invitation status from ${status} to accepted`;

            response.status(422).json({
                message
            });

        }

    } catch (error) {

        console.error(`failed to accept invitations: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};

/**
 * Pend team invitation
 *
 * @param {Request} request http request to pend team invitation
 * @param {Response} response http response
 * @returns {Promise<Response>} returns
 *  200  if invitation's status was updated to pending successfully
 *  500 { message }
 *
 */
export const pendTeamInvitation = async (request, response) => {

    try {

        const {params: {invitationId}} = request;

        await Invitation.findByIdAndUpdate({_id: invitationId}, {status: 'pending',
            updatedAt: Date.now()});
        response.sendStatus(200);

    } catch (error) {

        console.error(`failed to accept invitations: ${error}`);
        const {message} = error;

        response.
      status(500).
      json({
          message
      });

    }

};
