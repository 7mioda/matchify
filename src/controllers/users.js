import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import User from '../models/user';
import {sendRegistrationMail} from '../services/mailer';

/**
 * End-point to register a user
 *
 * @param {Request} request http request for user registration
 * @param {Response} response http response
 * @returns {Promise<Response>} returns
 *  200 { token, refreshToken } as Data if user was registered successfully
 *  422 { message } if user already exists
 *  500 { message }
 *
 */
export const register = async (request, response) => {

    try {

        const {
            body: {email, password}
        } = request,
            checkUser = await User.findOne({email});

        if (checkUser === null) {

            const hashedPassword = await bcrypt.hash(password, 10),
                newUser = new User({email,
                    password: hashedPassword}),
                user = await newUser.save(),
                token = jwt.sign({user: user.id}, 'app-super-secret', {
                    expiresIn: 1 // 1s
                }),
                refreshToken = jwt.sign(
              {user: user.id},
              `app-super-secret${user.password}`,
                    {
                        expiresIn: 604800 // Une semaine
                    }
            );

            await sendRegistrationMail('ahmed.edaly@esprit.tn', {email});
            response.json({
                token: `JWT ${token}`,
                refreshToken: `JWT ${refreshToken}`
            });

        } else {

            response.status(422).json({message: 'user already exists'});

        }

    } catch (error) {

        console.error(`Failed to insert new user: ${error}`);
        const {message} = error;

        response.status(500).json({message});

    }

};

/**
 *
 * @param request http request for user auth
 * @param response http response
 * @returns {Promise<Response>}
 *  200 { token, refreshToken } as Data if user was authenticated successfully
 *  401 { message } if auth params aren't valid
 *  500 { message }
 */
export const logIn = async (request, response) => {

    try {

        const {
            body: {email, password}
        } = request,
            user = await User.findOne({email}),
            {firstConnection} = user;

        if (firstConnection === null) {

            user.firstConnection = new Date();
            await user.save();

        }
        if (user != null && await bcrypt.compare(password, user.password)) {

            const token = jwt.sign({user: user.id}, 'app-super-secret', {
                    expiresIn: 1 // 1s
                }),
                refreshToken = jwt.sign(
              {user: user.id},
              `app-super-secret${user.password}`,
                    {
                        expiresIn: 604800
                    }
            );

            response.status(200).json({
                token: `JWT ${token}`,
                refreshToken
            });

        } else {

            response.sendStatus(401);

        }

    } catch (err) {

        console.error(`Failed to login: ${err}`);
        response.status(500).json({

        });

    }

};
