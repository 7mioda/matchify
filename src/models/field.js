import mongoose from 'mongoose';
import pricingSchema from './pricing';

const dimensionSchema = new mongoose.Schema({
        width: {
            type: Number,
            required: true
        },
        length: {
            type: Number,
            required: true
        }
    }),

    fieldSchema = new mongoose.Schema(
        {
            stadium: {type: mongoose.Schema.Types.ObjectId,
                ref: 'Stadium'},
            dimensions: {type: dimensionSchema,
                required: true},
            pricing: {
                type: pricingSchema
            },
            photos: [String],
            updatedAt: {type: Date,
                default: new Date()},
            createdAt: {type: Date,
                default: new Date()}
        },
        {
            collection: 'Field'
        }
);

fieldSchema.methods.toJSON = function userToJSON () {

    return {
        id: this._id,
        stadium: this.stadium,
        dimensions: this.dimensions,
        photos: this.photos,
        updatedAt: this.updatedAt,
        createdAt: this.createdAt
    };

};
export default mongoose.model('Field', fieldSchema);
