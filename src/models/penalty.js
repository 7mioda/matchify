import mongoose from 'mongoose';

const penaltySchema = new mongoose.Schema(
    {
        match: {type: mongoose.Schema.Types.ObjectId,
            ref: 'Match'},
        referee: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        description: String,
        fault: {type: mongoose.Schema.Types.ObjectId,
            ref: 'Fault'},
        goal: {type: mongoose.Schema.Types.ObjectId,
            ref: 'Gaol',
            default: null},
        createdAt: {type: Date,
            default: new Date()}
    },
    {
        collection: 'Penalty'
    }
);

export default mongoose.model('Penalty', penaltySchema);
