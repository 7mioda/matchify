import mongoose from 'mongoose';

const userSchema = new mongoose.Schema(
    {
        email: String,
        password: String,
        firstConnection: {type: Date,
            default: null},
        updatedAt: {type: Date,
            default: new Date()},
        createdAt: {type: Date,
            default: new Date()}
    },
    {
        collection: 'User'
    }
);

userSchema.methods.toJSON = function userToJSON () {

    return {
        id: this._id,
        email: this.email,
        createdAt: this.createdAt,
        updatedAt: this.updatedAt,
        firstConnection: this.firstConnection
    };

};
export default mongoose.model('User', userSchema);
