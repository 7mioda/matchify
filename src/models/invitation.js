import mongoose from 'mongoose';

const invitationSchema = new mongoose.Schema(
    {
        captain: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        member: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        team: {type: mongoose.Schema.Types.ObjectId,
            ref: 'Team'},
        status: {
            type: String,
            enum: [
                'pending',
                'refused',
                'accepted'
            ],
            default: 'pending'
        },
        updatedAt: {type: Date,
            default: new Date()},
        createdAt: {type: Date,
            default: new Date()}
    },
    {
        collection: 'Invitation'
    }
);

invitationSchema.methods.toJSON = function userToJSON () {

    return {
        id: this._id,
        captain: this.captain,
        member: this.member,
        team: this.team,
        updatedAt: this.updatedAt,
        createdAt: this.createdAt
    };

};
export default mongoose.model('Invitation', invitationSchema);
