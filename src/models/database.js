import mongoose from 'mongoose';
import configuration from '../../config';


const database = async () => {

    try {

        const environment = process.env.NODE_ENV || 'development',
            config = configuration[environment];

        mongoose.connection.
      on('error', (error) => {

          console.log(`MongoDB Connection error ${error}`);

      }).
      on('close', () => {

          console.log('Mongodb closed!');

      }).
      once('open', () => {

          console.log('connected to database');

      });

        console.log('MongoDB connection with retry');
        const {database: uri} = config;

        await mongoose.connect(uri, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useFindAndModify: false,
            socketTimeoutMS: 30000,
            keepAlive: true,
            reconnectTries: 30000
        });

        return true;

    } catch (err) {

        return err;

    }

};

export default database;
