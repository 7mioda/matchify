import mongoose from 'mongoose';
import locationSchema from './location';
import pricingSchema from './pricing';

const facilitySchema = new mongoose.Schema({
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        pricing: {
            type: pricingSchema
        },
        photos: [String]
    }),


    stadiumSchema = new mongoose.Schema(
        {
            owner: {type: mongoose.Schema.Types.ObjectId,
                ref: 'User'},
            location: {
                type: locationSchema,
                required: true
            },
            facilities: [{type: facilitySchema}],
            fields: [
                {type: mongoose.Schema.Types.ObjectId,
                    ref: 'Field'}
            ],
            photos: [String],
            updatedAt: {type: Date,
                default: new Date()},
            createdAt: {type: Date,
                default: new Date()}
        },
        {
            collection: 'Stadium'
        }
);

stadiumSchema.methods.toJSON = function userToJSON () {

    return {
        id: this._id,
        owner: this.owner,
        location: this.location,
        photos: this.photos,
        facilities: this.facilities,
        updatedAt: this.updatedAt,
        createdAt: this.createdAt
    };

};
export default mongoose.model('Stadium', stadiumSchema);
