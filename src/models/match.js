import mongoose from 'mongoose';

const scoreSchema = new mongoose.Schema({
        team: {type: mongoose.Schema.Types.ObjectId,
            ref: 'Team'},
        score: Number
    }),


    statsSchema = new mongoose.Schema({
        topScorer: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        topDefender: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        topPlayer: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        penalties: [
            {type: mongoose.Schema.Types.ObjectId,
                ref: 'Penalty'}
        ],
        faults: [
            {type: mongoose.Schema.Types.ObjectId,
                ref: 'Fault'}
        ],
        goals: [
            {type: mongoose.Schema.Types.ObjectId,
                ref: 'Goal'}
        ],

  /*
   * Offsides: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Offside' }],
   * kickoffs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Kickoff' }],
   * ThrowIns: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ThrowIn' }],
   */
        score: [
            {
                type: scoreSchema
            }
        ]
    }),

    matchSchema = new mongoose.Schema(
        {
            teams: [
                {type: mongoose.Schema.Types.ObjectId,
                    ref: 'Team'}
            ],
            referee: [
                {type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'}
            ],
            stadium: {type: mongoose.Schema.Types.ObjectId,
                ref: 'Stadium'},
            duration: {
                type: Number,
                enum: [
                    15,
                    30
                ],
                required: true
            },
            status: {
                type: String,
                enum: [
                    'planned',
                    'playing',
                    'paused',
                    'postponed'
                ],
                default: 'planned',
                required: true
            },
            goals: [
                {type: mongoose.Schema.Types.ObjectId,
                    ref: 'Goal'}
            ],
            date: [
                {type: Date,
                    required: true}
            ],
            score: [{type: scoreSchema}],
            stats: {type: statsSchema},
            updatedAt: {type: Date,
                default: new Date()},
            createdAt: {type: Date,
                default: new Date()}
        },
        {
            collection: 'Match'
        }
);

matchSchema.methods.toJSON = function userToJSON () {

    return {
        id: this._id,
        teams: this.teams,
        status: this.status,
        date: this.date,
        stadium: this.stadium,
        duration: this.duration,
        score: this.score,
        goals: this.goals,
        stats: this.stats,
        updatedAt: this.updatedAt,
        createdAt: this.createdAt
    };

};
export default mongoose.model('Match', matchSchema);
