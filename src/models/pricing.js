import mongoose from 'mongoose';

const pricingSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: [
            'perHr',
            'perMatch'
        ],
        required: true
    },
    price: {
        type: [Number],
        required: true
    }
});

export default pricingSchema;
