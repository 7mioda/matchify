import mongoose from 'mongoose';

const goalSchema = new mongoose.Schema(
    {
        match: {type: mongoose.Schema.Types.ObjectId,
            ref: 'Match'},
        referee: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        player: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        gradient: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        description: String,
        createdAt: {type: Date,
            default: new Date()}
    },
    {
        collection: 'Goal'
    }
);

export default mongoose.model('Goal', goalSchema);
