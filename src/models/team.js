import mongoose from 'mongoose';

const teamSchema = new mongoose.Schema(
    {
        captain: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        members: [
            {type: mongoose.Schema.Types.ObjectId,
                ref: 'User'}
        ],
        matches: [
            {type: mongoose.Schema.Types.ObjectId,
                ref: 'Match'}
        ],
        logo: String,
        updatedAt: {type: Date,
            default: new Date()},
        createdAt: {type: Date,
            default: new Date()}
    },
    {
        collection: 'Team'
    }
);

teamSchema.methods.toJSON = function userToJSON () {

    return {
        id: this._id,
        captain: this.captain,
        members: this.members,
        matches: this.matches,
        logo: this.logo,
        updatedAt: this.updatedAt,
        createdAt: this.createdAt
    };

};
export default mongoose.model('Team', teamSchema);
