import mongoose from 'mongoose';

const faultSchema = new mongoose.Schema(
    {
        match: {type: mongoose.Schema.Types.ObjectId,
            ref: 'Match'},
        referee: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        playerA: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        playerB: {type: mongoose.Schema.Types.ObjectId,
            ref: 'User'},
        card: {
            type: String,
            enum: [
                'red',
                'yellow',
                'none'
            ],
            default: 'none'
        },
        description: String,
        createdAt: {type: Date,
            default: new Date()}
    },
    {
        collection: 'Fault'
    }
);

export default mongoose.model('Fault', faultSchema);
