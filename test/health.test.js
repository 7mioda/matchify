import { Response } from 'jest-express/lib/response';
import { Request } from 'jest-express/lib/request';
import { testEndpoint } from "../src/controllers/serviceHealth";

let response;
let request;

describe('Test Endpoint', () => {
  beforeEach(() => {
    response = new Response();
    request = new Request();
  });

  afterEach(() => {
    response.resetMocked();
    request.resetMocked();
  });

  test('should return the wright response', async () => {
    request.setMethod('get');
    request.setBaseUrl('localhost:3000/users/test');
    await testEndpoint(request,response);
    expect(response.json).toBeCalledWith({
      data: 'OK'
    });
    expect(response.status).toBeCalledWith(200);
  });
});
