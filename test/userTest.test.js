import request from 'supertest';
import User from '../src/models/user'
import app from "../src/app";





describe('Users Endpoint', () => {
  afterAll(async (done) => {
    await User.deleteMany({});
    done();
  });

  test('should return 500 as status', async (done) => {
    request(app)
      .post('/api/v1/users/register')
      .expect(500)
      .end(function(err, res) {
        if (err) throw err;
        done();
      });
  });

  test('should register user', async (done) => {

    request(app)
      .post('/api/v1/users/register')
      .send('email=ahmededaly1993@gmail.com')
      .send('password=123')
      .expect(200)
      .end( async (err, res) => {
        if (err) throw err;
        const user = await User.findOne( { email: 'ahmededaly1993@gmail.com' } );
        console.log(user);
        expect(user).toBeTruthy();
        done();
      });
  });

  test('should login user', async (done) => {
    request(app)
      .post('/api/v1/users/login')
      .send('email=ahmededaly1993@gmail.com')
      .send('password=123')
      .expect(200)
      .end( async (err, res) => {
        if (err) throw err;
        done();
      });
  });

  test('should login user', async (done) => {
    request(app)
      .post('/api/v1/users/login')
      .send('email=ahmededaly1993@gmail.com')
      .send('password=wrong-password')
      .expect(401)
      .end( async (err, res) => {
        if (err) throw err;
        done();
      });
  });

  test('should return 422 user already exists', async (done) => {
    request(app)
      .post('/api/v1/users/register')
      .send('email=ahmededaly1993@gmail.com')
      .send('password=123')
      .expect(422)
      .end( async (err, res) => {
        if (err) throw err;
        done();
      });
  });
});
